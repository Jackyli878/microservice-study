import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def send_email(to_email, subject, body):
    from_email = "admin@conference.go"
    send_mail(subject, body, from_email, [to_email])


def process_approval(ch, method, properties, body):
    message = json.loads(body.decode())
    presenter_email = message["presenter_email"]
    presenter_name = message["presenter_name"]
    title = message["title"]
    subject = "Your presentation has been accepted"
    body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"
    send_email(presenter_email, subject, body)


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)


def process_rejection(ch, method, properties, body):
    message = json.loads(body.decode())
    presenter_email = message["presenter_email"]
    presenter_name = message["presenter_name"]
    title = message["title"]
    subject = "Your presentation has been rejected"
    body = f"{presenter_name}, we regret to inform you that your presentation {title} has been rejected"
    send_email(presenter_email, subject, body)


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()
